<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200203220148 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('ALTER TABLE user ADD COLUMN payment_data_id VARCHAR(150) NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TEMPORARY TABLE __temp__user AS SELECT id, first_name, last_name, telephone, street, house_number, zip_code, city, account_owner, iban FROM user');
        $this->addSql('DROP TABLE user');
        $this->addSql('CREATE TABLE user (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, first_name VARCHAR(40) NOT NULL, last_name VARCHAR(40) NOT NULL, telephone VARCHAR(20) NOT NULL, street VARCHAR(150) NOT NULL, house_number VARCHAR(20) NOT NULL, zip_code VARCHAR(15) NOT NULL, city VARCHAR(50) NOT NULL, account_owner VARCHAR(150) NOT NULL, iban VARCHAR(50) NOT NULL)');
        $this->addSql('INSERT INTO user (id, first_name, last_name, telephone, street, house_number, zip_code, city, account_owner, iban) SELECT id, first_name, last_name, telephone, street, house_number, zip_code, city, account_owner, iban FROM __temp__user');
        $this->addSql('DROP TABLE __temp__user');
    }
}