<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RegisterController extends AbstractController
{
    private $baseUrl;

    public function __construct(RequestStack $requestStack)
    {
        $this->baseUrl = $requestStack->getCurrentRequest()->getBaseUrl();
    }

    /**
     * @Route("/", methods={"GET"})
     */
    public function index()
    {
        return $this->render('registration/index.html.twig', ['baseUrl' => $this->baseUrl]);
    }

    /**
     * @Route("/", methods={"POST"})
     */
    public function userCreate(Request $request)
    {
        if ($request->cookies->has('form-values')) {
            $userData = (array) json_decode($request->cookies->get('form-values'));

            $user = new User();
            $user->setFirstName($userData['first-name']);
            $user->setLastName($userData['last-name']);
            $user->setTelephone($userData['telephone']);
            $user->setStreet($userData['street']);
            $user->setHouseNumber($userData['house-number']);
            $user->setCity($userData['city']);
            $user->setZipCode($userData['zip-code']);
            $user->setAccountOwner($userData['account-owner']);
            $user->setIban($userData['iban']);

            // Entity manager
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            // Get the last customer id added on the database via this request
            $customerId = $user->getId();

            // Make an api request to the payment gateway
            $apiResponse = $this->makeAPIRequest($customerId, $user->getIban(), $user->getAccountOwner());

            // Check the payment gateway response
            $responseType = ['baseUrl' => $this->baseUrl, 'msg' => '', 'paymentDataId' => ''];
            if ($apiResponse['statusCode'] == 200) {
                $paymentDataId = json_decode($apiResponse['content'], true)['paymentDataId'];

                $responseType['msg'] = 'The data has been saved successfully!';
                $responseType['paymentDataId'] = $paymentDataId;

                $em = $this->getDoctrine()->getManager();
                $user = $em->getRepository(User::class)->find($customerId);
                $user->setPaymentDataId($paymentDataId);
                $em->flush();
            } else {
                $responseType['msg'] = 'The request has been failed!';
            }

            // Return a response
            return $this->render('registration/feedback.html.twig', $responseType);
        }
    }

    public function makeAPIRequest($customerId, $iban, $accountOwner)
    {
        $client = HttpClient::create();

        $body = '{"customerId": "' . $customerId . '}", "iban": "' . $iban . '", "owner": "' . $accountOwner . '"}';
        $response = $client->request('POST', 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data', [
            "body" => $body,
        ]);

        // Reponse of API request
        return ['statusCode' => $response->getStatusCode(), 'content' => $response->getContent()];
    }

    /**
     * @Route("/save-state")
     */
    public function saveFormState(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $formView = $request->get('formView');
            $formValues = $request->get('formValues');

            $response = new Response();
            $response->headers->setCookie(new Cookie('form-view', $formView));
            $response->headers->setCookie(new Cookie('form-values', $formValues));
            $response->send();

            return new JsonResponse($request->cookies->all());
            // return new JsonResponse(['Cookie has been stored successfully!']);
        } else {
            return new JsonResponse(['Something went wrong!']);
        }
    }

    /**
     * @Route("get-state")
     */
    public function getFormState(Request $request)
    {
        $cookieArray = [];
        if ($request->cookies->has('form-view')) {
            $cookieArray['formView'] = $request->cookies->get('form-view');
        }
        if ($request->cookies->has('form-values')) {
            $cookieArray['formValues'] = $request->cookies->get('form-values');
        }
        return new JsonResponse($cookieArray);
    }
}