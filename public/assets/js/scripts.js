jQuery(document).ready(function() {
	getFormState();

	$('#top-navbar-1').on('shown.bs.collapse', function(){
    	$.backstretch("resize");
    });
    $('#top-navbar-1').on('hidden.bs.collapse', function(){
    	$.backstretch("resize");
    });

    // next step
    $('.registration-form .btn-next').on('click', function() {
    	var parent_fieldset = $(this).parents('fieldset');
		parent_fieldset.fadeOut(400, function() {
			$(this).next().fadeIn();
			saveFormState();
		});
    });

    // previous step
    $('.registration-form .btn-previous').on('click', function() {
    	$(this).parents('fieldset').fadeOut(400, function() {
			$(this).prev().fadeIn();
			saveFormState();
		});
	});

    $('.registration-form .btn, input').on('click, change', function() {
		saveFormState();
    });

    // submit
    $('.registration-form').on('submit', function(e) {
    	$(this).find('input[type="text"]').each(function() {
    		if( $(this).val() == "" ) {
    			e.preventDefault();
    			$(this).addClass('input-error');
    		}
    		else {
    			$(this).removeClass('input-error');
    		}
    	});
    });
});

function getBaseUrl()
{
	let urlArr = window.location.href.split("/");
	let domain = urlArr[0] + "//" + urlArr[2];
	let baseUrl = domain + $('meta[name=base-url]').attr("content");

	return baseUrl;
}

function getActiveView()
{
	let activeView;
	$("fieldset").each(function(){
		if($(this).css("display") == "block"){
			activeView = $(this).attr('id');
		}
	});
	return activeView;
}

function getFormState()
{
	let ajaxUrl = getBaseUrl() + "/get-state";
	$.ajax({
		url: ajaxUrl,
		type: "GET",
		dataType: "json",
		success: function(response){
			// Hide all views
			$("fieldset").fadeOut('slow');
			// Show the correct one
			if(typeof response.formView !== 'undefined') {
				$("fieldset").each(function(){
					if($(this).attr('id') == response.formView){
						$(this).fadeIn('slow');
					}
				});
			} else {
				$('.registration-form fieldset:first-child').fadeIn('slow');
			}
			// Set the stored values
			if(typeof response.formValues !== 'undefined') {
				let obj = JSON.parse(response.formValues);
				for (const property in obj) {
					$(`input[name="${property}"]`).val(obj[property]);
				}
			}
		}
	});
}

function saveFormState()
{
	let formView = getActiveView();
	let formValues = {};
	// loop to get values
	$('.registration-form').find('input[type="text"]').each(function() {
		if( $(this).val() !== "" ) {
			formValues[$(this).attr('name')] = $(this).val();
		}
	});

	let ajaxUrl = getBaseUrl() + "/save-state";
	$.ajax({
		url: ajaxUrl,
		type: "POST",
		data: { "formView": formView, "formValues": JSON.stringify(formValues) },
		dataType: "json",
		success: function(response){
			console.log(response);
		}
	});
}
