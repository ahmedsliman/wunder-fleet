# Wunder Fleet Registration

## Steps to install

- Composer install
- Sqlite driver must be installed: `sudo apt-get install php7.2-sqlite`
- SQLite file exists in var/wunder_fleet.sqlite and is should be writable

## Possible performance optimizations

### For frontend side

- Using cache for pages
- CDN for the assets files
- Minifying JS file
- Use new JS libs like react or vue that handleling state with a testable and proffesional solution instead of the current solution.

### For backend side

- Indexing many DB columns

## Which things could be done better

- Using SQLite instead of MYSQL for test purposes.
